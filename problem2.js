function groupingDataBasedOnCompanies(arr,cb){
    try{
        let groupingData=arr.reduce((acc,cv)=>{
        if(acc[cv.company]){
            acc[cv.company].push(cv);
        }else{
            acc[cv.company]=[];
            acc[cv.company].push(cv);
        }
        return acc;

    },{});
    cb(null,groupingData);
}catch(error){
    console.log(error);
}

};

module.exports=groupingDataBasedOnCompanies;