function sortingData(arr,cb){
    try{
    let sortData=arr.sort((a,b)=>{
        if(a.company===b.company){
            return a.id-b.id;
        }else{
            return a.company - b.company;
        }
    });

    cb(null,sortData);
}catch(error){
    console.log(error);
}
} 

module.exports=sortingData;