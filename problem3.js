function allDataOfPowerPuff(arr,cb){
    try{
    let powerPuffData=arr.reduce((acc,cv)=>{
        if(cv.company==="Powerpuff Brigade"){
            acc.push(cv);
        }
        return acc;
    },[]);

    cb(null,powerPuffData);
}catch(error){
    console.log(error);
}
}

module.exports=allDataOfPowerPuff;