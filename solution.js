const fs=require("fs");

let retrivingData=require("./problem1");
let groupingDataBasedOnCompanies=require("./problem2");
let allDataOfPowerPuff=require("./problem3");
let removeentryWithId2=require("./problem4");
let sortingData=require("./problem5");
let swappingPositions=require("./problem6");
let addingBirthDayDate=require("./problem7");


function main(){
    try{
    fs.readFile("./data.json","utf-8",(error,employeedata)=>{
        if(error){
            console.log(error);
        }else{
        
            console.log("Data read successfully");

            let employessData=JSON.parse(employeedata);
            let employees=employessData.employees;

            // Retriving Data

            retrivingData(employees,(error,retrivedata)=>{
                if(error){
                    console.log(error);
                }else{
                    fs.writeFile("./output/retriving-data.json",JSON.stringify(retrivedata),(error)=>{
                        if(error){
                            console.log(error);
                        }else{
                        
                            console.log("Data retrived successfully");

                            // Grouping data by companies

                            groupingDataBasedOnCompanies(employees,(error,data)=>{
                                if(error){
                                    console.log(error);
                                }else{
                                    fs.writeFile("./output/grouping-data-by-companies.json",JSON.stringify(data),(error)=>{
                                        if(error){
                                            console.log(error);
                                        }else{
                                    
                                            console.log("Data grouped by companies");

                                            // Data of Power Puff company

                                            allDataOfPowerPuff(employees,(error,data)=>{
                                                if(error){
                                                    console.log(error);
                                                }else{
                                                    fs.writeFile("./output/data-of-power-puff.json",JSON.stringify(data),(error)=>{
                                                        if(error){
                                                            console.log(error)
                                                        }else{
                                                        
                                                            console.log("Power Puff data");

                                                            // Removing id2 data

                                                            removeentryWithId2(employees,(error,data)=>{
                                                                if(error){
                                                                    console.log(error);
                                                                }else{
                                                                    fs.writeFile("./output/removing-entry-with-id2.json",JSON.stringify(data),(error)=>{
                                                                        if(error){
                                                                            console.log(error);
                                                                        }else{
                                                                    
                                                                            console.log("id removed successfully");

                                                                            //sorting data

                                                                            sortingData(employees,(error,data)=>{
                                                                                if(error){
                                                                                    console.log(error);
                                                                                }else{
                                                                                    fs.writeFile("./output/sorting-data.json",JSON.stringify(data),(error)=>{
                                                                                        if(error){
                                                                                            console.log(error);
                                                                                        }else{
                                                                                    
                                                                                            console.log("sorted data");

                                                                                            // Swaping id with 92 and 93

                                                                                            swappingPositions(employees,93,92,(error,data)=>{
                                                                                                if(error){
                                                                                                    console.log(error);
                                                                                                }else{
                                                                                                    fs.writeFile("./output/swapped-data.json",JSON.stringify(data),(error)=>{
                                                                                                        if(error){
                                                                                                            console.log(error);
                                                                                                        }else{
                                                                                                            
                                                                                                            console.log("data swapped");

                                                                                                            // Adding birth day date to even ids

                                                                                                            addingBirthDayDate(employees,(error,data)=>{
                                                                                                                if(error){
                                                                                                                    console.log(error)
                                                                                                                }else{
                                                                                                                    fs.writeFile("./output/adding-DOB.json",JSON.stringify(data),(error)=>{
                                                                                                                        if(error){
                                                                                                                            console.log(error);
                                                                                                                        }else{
                                                                                                                            console.log("DOB added");
                                                                                                                        }
                                                                                                                    })
                                                                                                                }
                                                                                                            })
                                                                                                        }
                                                                                                    })
                                                                                                }
                                                                                            })
                                                                                        }
                                                                                    })
                                                                                }
                                                                            })
                                                                        }
                                                                    })
                                                                }
                                                            })
                                                        }
                                                    })
                                                }
                                            })


                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            })
        }
    })
}catch(error){
    console.log(error);
}
}

main()