function swappingPositions(arr,id1,id2,cb){
    try{
    let index1=arr.findIndex((value)=> value.id===id1);
    let index2=arr.findIndex((value)=> value.id===id2);

    if(index1 !== -1 && index2 !== -1){
        [arr[index1],arr[index2]]=[arr[index2],arr[index1]];
    }else{
        console.log("ids not present");
    }

    cb(null,arr);
}catch(error){
    console.log(error);
}

    
}

module.exports=swappingPositions;